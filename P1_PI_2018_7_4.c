/*Omogućiti korisniku unos dimenzija kvadratne matrice (2-D polja) m × m, gdje
je 5 < m < 25 i m mora biti neparan broj. Popuniti matricu parnim pseudo-slučajnim
brojevima iz [0, 255] ⊂ Z. Zamijeniti vrijednost elementa koji se nalazi na 
sjecištu dijagonala novom vrijednošću koja se računa kao aritmetička sredina svih
susjednih elemenata tog broja (susjedne elemente čini osam elemenata oko jednog 
centralnog).*/

#include <stdlib.h>
#include <time.h>
#include <stdio.h>

int main(void) {
	int matrica[23][23]; //23 je najveci neparni broj u intervalu <5, 25>
	int m;

	do {
		scanf("%d", &m);
	} while (m <= 5 || m >= 25 || m % 2 == 0); // ponavljaj dok god je neki od ovih uvjeta ispunjen

	srand((unsigned)time(NULL));
	int i;
	for (i = 0; i < m; i++) {
		matrica[i] = 2 * rand() % 128; // alternativna formula je 0 + 1.0 * rand() / RAND_MAX * (127 - 0) * 2
	}

	int ind_sjecista = m / 2;
	int suma = 0;
	int j;
	for (i = ind_sjecista - 1; i <= ind_sjecista + 1; i++) {
		for (j = ind_sjecista - 1; j <= ind_sjecista + 1; j++) {
			if (i != ind_sjecista && j != ind_sjecista){
				suma = suma + matrica[i][j];
			}
		}
	}
	matrica[ind_sjecista][ind_sjecista] = suma / 8;

	return 0;
}
/*Napisati funkciju koja od predanog joj cijelog broja određuje i vraća novi broj
kojemu su izbačene parne znamenke. Primjerice, za broj 78635 funkcija treba 
vratiti broj 735. U svrhu testiranja u funkciji main() pozvati napisanu funkciju
s brojem 569213 kao argumentom i na ekran ispisati povratnu vrijednost. Funkcija
za potenciranje (x^p) opisana je u zaglavnoj datoteci math.h, a prototip joj je
double pow(double x, double p).*/

#include <stdio.h>

int funk(int ulaz) {
	int znamenka, izlaz = 0, mnozitelj = 1;
	while (ulaz != 0) {
		znamenka = ulaz % 10; 
		ulaz = ulaz / 10; 

		if (znamenka % 2 == 1) {
			izlaz += znamenka * mnozitelj;
			mnozitelj *= 10;
		}
	}
	return izlaz;
}

int main(void) {
	printf("%d", funk(569213));
	return 0;
}

/*Napisati funkciju koja nasumično premješta brojeve unutar prednog joj cjelobrojnog
polja. Premještanje se obavlja na sljedeći način: redom svaki element zamijenjuje s
nasumično odabranim elementom polja. Izdvojiti korak zamjene dva elementa polja u 
posebnu funkciju. U svrhu testiranja u funkciji main() dinamički zauzeti memoriju 
za 45 podataka tipa short int (u potpunosti rukovati memorijom). Popuniti navedeno 
polje pseudo-slučajnim brojevima iz [−100, 100] ⊂ Z. Pozvati navedenu funkciju 
s tim poljem kao argumentom i naknadno ga ispisati na ekran.*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

void zamjeni (short int *prvi, short int *drugi) {
	short int privremeni = *prvi;
	*prvi = *drugi;
	*drugi = privremeni;
}

void funk(short int *polje, int velicina) {

	int i;
	int novi_i;
	for (i = 0; i < velicina; i++) {
		novi_i = rand() % velicina;
		zamjeni(&polje[i], &polje[novi_i]);
	}
}

int main(void) {
	short int *polje = (short int*) calloc(45, sizeof(short int));

	if (NULL == polje) {
		printf("GRESKA");
		return 1;
	}

	srand((unsigned)time(NULL));
	int i;
	for (i = 0; i < 45; i++) {
		polje[i] = -100 + (rand() % (100 - (-100) + 1));
	}
	funk(polje, 45);
	for (i = 0; i < 45; i++){
		printf("%3d\t", polje[i]);
	}

	free(polje);
	return 0;
}

/*Napisati funkciju koja računa i vraća 
	f(x) = e^(0.1 * (x_1 − x_n)) + product^(n−2)_(i=2)(|x_i − x_(i+1)|).
U svrhu testiranja u funkciji main() deklarirati polje realnih brojeva od 20 
elemenata i naknadno ga popuniti pseudo-slučajnim brojevima iz [−10, 10] ⊂ R. 
Pozvati funkciju s navedenim poljem kao argumentom i ispisati na ekran povratnu 
vrijednost. Funkcija za izračun vrijednosti eksponencijalne funkcije [f(z) = e^z] 
opisana je u zaglavnoj datoteci math.h, a prototip joj je double exp(double z)*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

double aps(double x) {
	if (x < 0){
		return -1 * x;
	} else {
		return x;
	}
}

double funk(double *x, int n) {
	double rezultat = exp(0.1 * (x[0] - x[n - 1]));
	double produkt = 1;
	int i;
	for (i = 1; i < velicina - 2; i++) {
		produkt *= aps(x[i] - x[i+1]);
	}
	rezultat = rezultat + produkt;
}

int main(void) {
	double polje[20];
	int i;
	srand((unsigned)time(NULL));
	for (i = 0; i < 20; i++) {
		polje[i] = -10 + 1.0 * rand() / RAND_MAX * (10 - (-10));
	}
	printf("%lf", funk(polje, 20));
	return 0;
}

/*Napisati funkciju koja iz predanog joj stringa određuje skraćenicu i ispisuje
ju na ekran. Skraćenica će biti niz znakova koji predstavljaju prvo slovo svake
riječi u stringu osim onih riječi koje imaju samo jedno slovo. Primjerice, za 
string "Fakultet elektrotehnike, računarstva i informacijskih tehnologija" 
funkcija treba ispisati Ferit. U svrhu testiranja u funkciji main() dinamički 
zauzeti memoriju za 48 podataka tipa char (u potpunosti rukovati memorijom). 
Omogućiti korisniku unos stringa (osigurati da se ne premaši veličina prethodno
zauzetog polja). Pozvati funkciju s tim stringom kao argumentom.*/

#include <stdlib.h>
#include <stdio.h>

void funk(char *polje){
	int i;
	for (i = 0; polje[i] != '\0'; i++) {
		if (i == 0) //TODO
	}
}

int main(void) {
	char *neki_string = (char*) calloc(48, sizeof(char));
	if (NULL == neki_string) {
		printf("GRESKA");
		return 1;
	}
	fgets(neki_string, 48, stdin);
	funk(neki_string);
	free(neki_string);
	return 0;
}